#include "lib_lista_complementar.h"
#include <stdlib.h>

void imprime_lista(t_lista *l) {
	int x;

	if (lista_vazia (l))
		return;

	inicializa_atual_inicio (l);
	while (consulta_item_atual (&x, l)) {
		printf("%d ", x);
		incrementa_atual (l);
	}
	printf ("\n");
}

int copia_lista(t_lista *l, t_lista *c) {
	int x;

	if (lista_vazia (l))
		return 0;

	/* Garante que a lista esteja vazia para realizar a cópia */
	destroi_lista (c);
	inicializa_lista (c);
	inicializa_atual_inicio (l);
	while (consulta_item_atual (&x, l)) {
		incrementa_atual (l);
		insere_fim_lista (x, c);
	}
	return 1;
}

int concatena_listas(t_lista *l, t_lista *c) {
	int x;

	if (lista_vazia (l) || lista_vazia (c))
		return 0;

	inicializa_atual_fim (l);
	inicializa_atual_inicio (c);
	while (consulta_item_atual (&x, c)) {
		insere_fim_lista (x, l);
		incrementa_atual (c);
	}
	destroi_lista (c);
	return 1;
}

int ordena_lista(t_lista *l) {
	int x;
	t_lista list_aux;

	if (lista_vazia (l))
		return 0;

	/* Usa uma lista auxiliar para ordenar a lista "l" */
	inicializa_lista (&list_aux);
	inicializa_atual_inicio (l);
	while (consulta_item_atual (&x, l)) {
		insere_ordenado_lista (x, &list_aux);
		incrementa_atual (l);
	}

	/* Copia a lista ordenada na lista recebida pela função */
	copia_lista (&list_aux, l);
	destroi_lista (&list_aux);
	return 1;
}

int intercala_listas(t_lista * l, t_lista * m, t_lista * i) {
	if (lista_vazia (l) || lista_vazia (m))
		return 0;

	int elemento_l, elemento_m;

	/* Garantia que as listas estarão ordenadas */
	ordena_lista (l);
	ordena_lista (m);

	inicializa_atual_inicio (l);
	inicializa_atual_inicio (m);
	inicializa_lista (i);
	
	/* Enquanto não atingir o fim de uma das listas, compara os elementos atuais e insere o menor na lista de saída */
	while (consulta_item_atual (&elemento_l, l) && consulta_item_atual (&elemento_m, m)) {
		if (elemento_l < elemento_m) {
			insere_fim_lista (elemento_l, i);
			incrementa_atual (l);
		}
		else {
			insere_fim_lista (elemento_m, i);
			incrementa_atual (m);
		}
	}

	/* Verifica qual das listas ainda tem elementos e os adiciona na lista de saída */
	while (consulta_item_atual (&elemento_l, l)) {
		insere_fim_lista (elemento_l, i);
		incrementa_atual (l);
	}
	while (consulta_item_atual (&elemento_m, m)) {
		insere_fim_lista (elemento_m, i);
		incrementa_atual (m);
	}
	return 1;
}
