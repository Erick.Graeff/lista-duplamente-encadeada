#include <stdio.h>
#include "lib_lista_complementar.h"

#define MAX 5

int testa_inicializacao (t_lista * l_1, t_lista * l_2, t_lista * l_3, t_lista * l_4, t_lista * l_5);
void testa_insercoes (t_lista * l_1, t_lista * l_2, t_lista * l_3);
int testa_insere_ordenado (t_lista * l);
int testa_insercao_ini (t_lista * l);
int testa_insercao_fim (t_lista * l);
void testa_copia (t_lista * l, t_lista * c);
void testa_concatenacao (t_lista * l, t_lista * c);
void testa_ordenacao (t_lista * l);
void testa_intercala (t_lista * l, t_lista * m, t_lista * i);
void testa_destruicao (t_lista * l_1, t_lista * l_2, t_lista * l_3, t_lista * l_4, t_lista * l_5);




int main () {
	t_lista list_1, list_2, list_3, list_4, list_5;

	if (! testa_inicializacao (&list_1, &list_2, &list_3, &list_4, &list_5)) {
		printf ("Não foi possível iniciar lista\n");
		return 0;
	}

	testa_insercoes (&list_1, &list_2, &list_3);
	testa_copia (&list_1, &list_4);
	testa_concatenacao (&list_2, &list_3);
	testa_ordenacao (&list_2);
	testa_intercala (&list_1, &list_2, &list_5);
	testa_destruicao (&list_1, &list_2, &list_3, &list_4, &list_5);


	return 0;
}




int testa_inicializacao (t_lista * l_1, t_lista * l_2, t_lista * l_3, t_lista * l_4, t_lista * l_5) {
	printf ("Inicializando lista 1\n");
	if (! inicializa_lista (l_1))
		return 0;

	printf ("Inicializando lista 2\n");
	if (! inicializa_lista (l_2))
		return 0;

	printf ("Inicializando lista 3\n");
	if (! inicializa_lista (l_3))
		return 0;

	printf ("Inicializando lista 4\n");
	if (! inicializa_lista (l_4))
		return 0;
	return 1;
}

int testa_insercao_ini (t_lista * l) {
	int i, elemento, tam;
	printf ("\nDigite o tamanho de sua lista: ");
	scanf ("%d", &tam);
	printf("Digite os inteiros que você gostaria de adicionar: ");
	/* Lê e insere na lista */
	for (i = 1; i <= tam; i++) {
		scanf ("%d", &elemento);
		if (! insere_inicio_lista (elemento, l))
			printf("Elemento %d não inserido\n", elemento);
	}
	return 1;
}

int testa_insercao_fim (t_lista * l) {
	int i, elemento, tam;
	printf ("\nDigite o tamanho de sua lista: ");
	scanf ("%d", &tam);
	printf("Digite os inteiros que você gostaria de adicionar: ");
	/* Lê e insere na lista */
	for (i = 1; i <= tam; i++) {
		scanf ("%d", &elemento);
		if (! insere_fim_lista (elemento, l))
			printf("Elemento %d não inserido\n", elemento);
	}
	return 1;
}

int testa_insere_ordenado (t_lista * l) {
	int i, elemento, tam;
	printf("\nDigite o tamanho de sua lista: ");
	scanf ("%d", &tam);
	printf("Digite os inteiros que você gostaria de adicionar: ");
	/* Lê e insere na lista */
	for (i = 1; i <= tam; i++) {
		scanf ("%d", &elemento);
		if (! insere_ordenado_lista (elemento, l))
			printf("Elemento %d não inserido\n", elemento);
	}
	return 1;
}

void testa_insercoes (t_lista * l_1, t_lista * l_2, t_lista * l_3) {
	/* Inserindo elemento no início da lista */
	printf ("\nInsira no início da lista 1");
	testa_insercao_ini (l_1);
	printf("Lista 1 -> ");
	imprime_lista (l_1);

	/* Inserindo elemento no fim da lista */
	printf ("\nInsira no final da lista 2");
	testa_insercao_fim (l_2);
	printf("Lista 2 -> ");
	imprime_lista (l_2);

	/* Inserindo elementos de forma ordenada na lista */
	printf("\nInserindo ordenado na lista 3");
	testa_insere_ordenado (l_3);
	printf("Lista 3 -> ");
	imprime_lista (l_3);
}

/*void testa_remocoes (t_lista * l_1, t_lista * l_2, t_lista * l_3) {
	int x, i; */

	/* Removendo elemento do início da lista */
	/*printf ("\nTestando remoção do item que está no início da lista 1\n");
	if (remove_inicio_lista (&x, l_1)) {
		printf ("Item removido: %d\n", x);
		imprime_lista (l_1);
	}
	else
		printf ("Não foi possível remover\n");*/

	/* Removendo elemento do fim da lista */
	/*printf ("\nTestando remoção do item que está no fim da lista 2\n");
	if (remove_fim_lista (&x, l_2)) {
		printf("Item removido: %d\n", x);
		imprime_lista (l_2);
	}
	else
		printf("Não foi possível remover\n");*/

	/* Removendo elemento solicitado da lista */
	/*int quantidade, elemento;

	printf ("\nInforme quantos inteiros gostaria de remover da lista 3: ");
	scanf ("%d", &quantidade);
	printf("\nInforme quais inteiros gostaria de remover: \n");
	for (i = 1; i <= quantidade; i++) {
		scanf ("%d", &elemento);
		if (remove_item_lista (elemento, &x, l_3))
			printf ("Item removido: %d\n", x);
		else
			printf ("Não foi possível remover %d\n", elemento);
	}
	imprime_lista (l_3);
}*/

void testa_copia (t_lista * l, t_lista * c) {
	printf ("\nTestando cópia: copiando lista 1 na lista 4\n");
	if (copia_lista (l, c)) {
		printf("Lista 4 -> ");
		imprime_lista (c);
	}
	else
		printf ("Cópia falhou\n");
}

void testa_concatenacao (t_lista * l, t_lista * c) {
	printf ("\nTestando concatenação: concatenando lista 3 na lista 2\n");
	if (concatena_listas (l, c)) {
		printf("Lista 2 -> ");
		imprime_lista (l);
	}
	else
		printf ("Concatenação falhou\n");
}

void testa_ordenacao (t_lista * l) {
	printf ("\nTestando ordenação na lista 2\n");
	if (ordena_lista (l)) {
		printf("Lista 2 -> ");
		imprime_lista (l);
	}
	else
		printf ("Ordenação falhou\n");
}

void testa_intercala (t_lista * l, t_lista * m, t_lista * i) {
	printf ("\nTestando intercalação entre a lista 1 e a lista 2\n");
	if (intercala_listas (l, m, i)) {
		printf("Lista 5 -> ");
		imprime_lista (i);
	}
	else
		printf ("Intercalação falhou");
}

void testa_destruicao (t_lista * l_1, t_lista * l_2, t_lista * l_3, t_lista * l_4, t_lista * l_5) {
	printf ("\nDestruindo listas\n");
	destroi_lista (l_1);
	imprime_lista (l_1);

	destroi_lista (l_2);
	imprime_lista (l_2);

	/* Essa lista foi destruída na função "testa_concatenacao", apresentará mensagem de erro */
	destroi_lista (l_3);
	imprime_lista (l_3);

	destroi_lista (l_4);
	imprime_lista (l_4);

	destroi_lista (l_5);
	imprime_lista (l_5);
	printf ("Listas destruídas\n");
}