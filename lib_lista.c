#include <stdio.h>
#include <stdlib.h>
#include "lib_lista_complementar.h"

int inicializa_lista (t_lista *l) {
	t_nodo *sent_ini, *sent_fim;

	sent_ini = (t_nodo *) malloc (sizeof (t_nodo));
	if (sent_ini == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}
	sent_fim = (t_nodo *) malloc (sizeof (t_nodo));
	if (sent_fim == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}

	l->ini = sent_ini;
	l->fim = sent_fim;
	sent_ini->prox = l->fim;
	sent_fim->prev = l->ini;
	sent_ini->prev = NULL;
	sent_fim->prox = NULL;
	l->atual = NULL;
	l->tamanho = 0;
	return 1;
}

int lista_vazia (t_lista *l) {
	if (!l->tamanho)
		return 1;
	return 0;
}

void destroi_lista (t_lista *l) {
	if (l->tamanho == 0 && l->ini == NULL && l->fim == NULL && l->atual ==NULL) {
		printf ("Lista já foi destruída\n");
		return;
	}

	t_nodo *aux = l->ini;
	l->atual = aux->prox;

	while (l->atual != l->fim->prox) {
		free (aux);
		aux = l->atual;
		l->atual = l->atual->prox;
	}
	free (aux);
	l->ini = NULL;
	l->fim = NULL;
	l->atual = NULL;
	l->tamanho = 0;
}

int insere_inicio_lista (int item, t_lista *l) {
	t_nodo * new;
	new = (t_nodo *) malloc (sizeof (t_nodo));
	if (new == NULL) {
		printf ("Não há memória suficiente. ");
		return 0;
	}
	new->chave = item;
	new->prev = l->ini;
	new->prox = l->ini->prox;
	l->ini->prox = new;
	new->prox->prev = new;
	l->atual = new;
	l->tamanho++;
	return 1;
}

int tamanho_lista (int *tam, t_lista *l) {
	if (lista_vazia (l))
		return 0;

	*tam = l->tamanho;
	return 1;
}

int insere_fim_lista (int item, t_lista *l) {
	if (lista_vazia (l))
		return insere_inicio_lista (item, l);

	t_nodo * new;
	new = (t_nodo *) malloc (sizeof (t_nodo));
	if (new == NULL) {
		printf ("Não há memória suficiente. ");
		return 0;
	}

	new->chave = item;
	l->atual = l->fim->prev;
	new->prev = l->atual;
	new->prox = l->fim;
	l->atual->prox = new;
	l->fim->prev = new;
	l->tamanho++;
	return 1;
}

int insere_ordenado_lista (int item, t_lista *l) {

	/* Caso o lugar do novo elemento seja na primeira posição */
	if (lista_vazia(l))
		return insere_inicio_lista (item, l);

	/* Caso a lista só tenha um elemento */
	if (l->ini->prox == l->fim) {
		if (l->ini->chave > item)
			return insere_inicio_lista (item, l);
		return insere_fim_lista (item, l);
	}

	l->atual = l->ini->prox;
	while ((l->atual->chave <= item) && (l->atual->prox != l->fim))
		l->atual = l->atual->prox;

	/* Caso o lugar do novo elemento seja na última posição */
	if (l->atual->prox == l->fim && l->atual->chave <= item)
		return insere_fim_lista (item, l);

	t_nodo * new;
	new = (t_nodo *) malloc (sizeof (t_nodo));
	if (new == NULL) {
		printf ("Não há memória suficiente. ");
		return 0;
	}

	new->chave = item;
	new->prev = l->atual->prev;
	new->prox = l->atual;
	l->atual->prev->prox = new;
	l->atual->prev = new;
	l->tamanho++;
	return 1;
}

int remove_inicio_lista (int *item, t_lista *l) {
	if (lista_vazia (l))
		return 0;

	l->atual = l->ini->prox;
	*item = l->atual->chave;
	t_nodo *aux = l->atual;
	l->atual->prox->prev = l->atual->prev;
	l->ini->prox = l->atual->prox;
	free (aux);
	l->tamanho--;
	return 1;
}

int remove_fim_lista (int *item, t_lista *l) {
	if (lista_vazia (l))
		return 0;

	l->atual = l->fim->prev;
	*item = l->atual->chave;
	t_nodo *aux = l->atual;
	l->atual->prev->prox = l->atual->prox;
	l->fim->prev = l->atual->prev;
	free (aux);
	l->tamanho--;
	return 1;
}

int remove_item_lista (int chave, int *item, t_lista *l) {
	if (lista_vazia (l))
		return 0;

	if (l->ini->prox->chave == chave)
		return remove_inicio_lista (item, l);

	if (l->fim->prev->chave == chave)
		return remove_fim_lista (item,l);

	/* Caso a chave não seja igual ao ultimo ou primeiro elemento */
	l->atual = l->ini->prox->prox;
	while (l->atual->chave != chave && l->atual->prox != l->fim->prev)
		l->atual = l->atual->prox;

	if (l->atual->chave != chave)
		return 0;

	/* Caso a chave tenha sido achada */
	t_nodo * aux = l->atual;
	*item = l->atual->chave;
	l->atual->prev->prox = l->atual->prox;
	l->atual->prox->prev = l->atual->prev;
	free (aux);
	l->tamanho--;
	return 1;
}

int pertence_lista (int chave, t_lista *l) {
	if (lista_vazia (l))
		return 0;

	if (l->ini->prox->chave == chave || l->fim->prev->chave == chave)
		return 1;

	l->atual = l->ini->prox->prox;
	while ((l->atual->chave != chave) && (l->atual->prox != l->fim->prev))
		l->atual = l->atual->prox;

	if (l->atual->chave == chave)
		return 1;
	return 0;
}

int inicializa_atual_inicio (t_lista *l) {
	if (lista_vazia (l))
		return 0;

	l->atual = l->ini->prox;
	return 1;
}

int inicializa_atual_fim (t_lista *l) {
	if (lista_vazia (l))
		return 0;

	l->atual = l->fim->prev;
	return 1;
}

void incrementa_atual (t_lista *l) {
	if (lista_vazia (l))
		return;

	if (l->atual == l->fim)
		return;

	l->atual = l->atual->prox;
}

void decrementa_atual (t_lista *l) {
	if (lista_vazia (l))
		return;

	if (l->atual->prev == l->ini)
		return;

	l->atual = l->atual->prev;
}

int consulta_item_atual (int *item, t_lista *l) {
	if (lista_vazia (l))
		return 0;

	if (l->atual == NULL)
		return 0;

	if (l->atual == l->ini || l->atual == l->fim)
		return 0;

	*item = l->atual->chave;
	return 1;
}

int remove_item_atual (int *item, t_lista *l) {
	if (lista_vazia(l))
		return 0;

	if (l->atual == NULL)
		return 0;

	if (l->atual == l->ini || l->atual == l->fim)
		return 0;

	t_nodo *aux = l->atual;
	*item = l->atual->chave;
	l->atual->prox->prev = l->atual->prev;
	l->atual->prev->prox = l->atual->prox;
	free (aux);
	l->tamanho--;
	return 1;
}
